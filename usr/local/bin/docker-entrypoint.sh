#!/bin/bash

set -e

if [ "$1" = 'grafana' ]; then
    exec grafana server -config /etc/grafana/grafana.ini -homepath $GRAFANA_HOME
fi

exec "$@"
