#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=grafana
ENV APP_GROUP=grafana

ENV GRAFANA_VERSION="11.2.0"

ENV BASE_PKGS libfontconfig1 musl

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*

##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS build-essential wget

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS} \
 && rm -rf /var/lib/apt/lists/*

# Run build
WORKDIR /usr/src
RUN wget -nv https://dl.grafana.com/enterprise/release/grafana-enterprise-${GRAFANA_VERSION}.linux-amd64.tar.gz
RUN mkdir grafana
RUN tar -zxC ./grafana --strip-components 1 -f grafana-enterprise-${GRAFANA_VERSION}.linux-amd64.tar.gz
RUN mv grafana /usr/local


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV GRAFANA_HOME="/usr/local/grafana"
ENV PATH="$GRAFANA_HOME/bin:$PATH"

RUN mkdir /usr/local/grafana/plugins \
 && mkdir -p /var/lib/grafana/logs \
 && chown -R ${APP_USER} /var/lib/grafana


EXPOSE 3000/tcp
VOLUME ["/etc/grafana", "/var/lib/grafana" ]

USER ${APP_USER}
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["grafana"]
